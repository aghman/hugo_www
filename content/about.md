---
title: "About"
date: 2020-11-26T16:33:35-08:00
draft: false
menu: "main"
---


## About Me

I'm Austin. The guy who runs this site. And who named it. But you might have figured that one out already.  But who am I? 

Since I was a little kid, I've been using, tinkering, and otherwise nerding out on computers. My first was an [IBM PC XT](https://en.wikipedia.org/wiki/IBM_Personal_Computer_XT), my most recent is a Macbook Pro that contains Apple's new fancy-pants ARM processors.  These days, I'm a professional nerd. A software engineer if you will. 
But that's not all there is to me. Music fills me with joy.  I love to travel. To cook. To garden. You should see me with a set of pruners.  

I made this site to share all the things I love with anyone who might be interested.  I hope a few things I put out in the world makes you smile or helps you learn something.  So enjoy your stay and come back to see what I'm up to.  

If you want to see what I've done professionally, check out my [Linkedin profile](https://www.linkedin.com/in/austinghill).  It's not nearly as interesting as the rest, but it pays the bills. 

**-Austin**

## About This Site

This site is statically generated with [Hugo](https://gohugo.io), and hosted on [Hostineer](https://hostineer.com). 