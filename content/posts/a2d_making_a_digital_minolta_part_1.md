---
title: "A2D - Making A Digital Minolta, part 1"
date: 2020-12-26T22:21:14-08:00
draft: false
tags:
- Cameras
- Photography
- Digital Minolta
---
![Austin, in his younger days, with his Minolta SRT202](/images/minolta/austin_with_srt202.png "Austin, in his younger days, with his Minolta SRT202")

## A Boy and His Camera
My first camera was a hand-me-down Minolta SRT-202 that my parents gave me.  They bought it in the late 70s, used it and loved it, and passed it down to me when I showed an interest in photography.  It was my first, and is still my favorite camera.  It was sturdy, fit my hands perfectly, and has a wonderful interface.  

But, I don't shoot film any more these days; it's too expensive and I'm far too lazy to scan images.  But I still love this camera.  Ever since digital cameras started to get cheaper, I longed for a digital version of my beloved SRT.  Now is the time to make it a reality.  

In the beginning of the pandemic, I picked up a Raspberry Pi 4 on a whim.  Little did I know it'd be the gateway drug to the larger world of tiny, single board computers and the world of making.  After picking up a Raspberry Pi Zero W, hearing about the introduction of the Raspberry Pi HQ camera, with it's C-mount interface, I started to realize that I could possibly combine these technologies to make my old dream of a digital SRT a reality.  

So what are my hopes and dreams around what this camera should do, how it behaves, and how you'd interact with it? 

## What Should It Do?

1. The shutter button causes an image to be captured.  
![SRT 202 Shutter Button](/images/minolta/SRT202_Shutter_Button.png "SRT 202 Shutter Button")
2. Standard MC/MD lenses will attach to the front, as with a standard film version.  ![SRT 202 Lens Attachment](/images/minolta/SRT202_Lens_Attachment.png "SRT 202 Lens Attachment")
3. The camera turns on and off with the power knob on the bottom. ![SRT 202 Power Knob](/images/minolta/SRT202_Power_Switch.png "SRT 202 Power Knob")
4. The viewfinder shows the viewer the light(the image) coming into the front of the body. ![SRT 202 Viewfinder](/images/minolta/SRT202_Viewfinder.png "SRT 202 Viewfinder")
5. The camera is battery powered and can be recharged via USB. 
6. I can visually determine the battery level. 
7. I can see photos after they are taken, on a screen.


## Bonus Points


1. I can view and manage photos on a screen that's part of the camera. 


Now, I'm sure I'll come up with more as I start working.  I hope all these goals are in the realm of possibility, but a guy can dream, right?  Also, I'm not planning on using my existing SRT (for now) for the investigation and prototyping. Instead, I'm going to be picking up some bodies on eBay or other cheap sites. I'll put up photos when I start getting them in my hot little hands. 

## Note about photos in this post

The first photo was taken by me (when I had hair!) of the SRT202, by the SRT202.  I had it scanned in when the film was developed. 

The rest of the photos were taken by my iPhone 11 Pro camera, which made the viewfinder shot possible.  