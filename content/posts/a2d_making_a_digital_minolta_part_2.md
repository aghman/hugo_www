---
title: "A2D - Making A Digital Minolta - Prototype 1 - Taking A Picture"
date: 2020-12-20T22:21:14-08:00
draft: true
tags:
- Cameras
- Photography
- Digital Minolta
- Research
- Single Board Computers
---

What's the most basic thing that a camera does? You press a button and capture a photo. That was my first major step. Sounds simple, right?  Luckily, on many levels, it is.  Since I'm using the Raspberry Pi and the Pi HQ Camera module, I already have a set of easy-to-use libraries and executables that allow me to programmatically capture a photo. But what about the button-press part? 

![SRT 202 Shutter Button](/images/minolta/SRT202_Shutter_Button.png "SRT 202 Shutter Button")

## A One-Press Wonder

The SRT is a relatively simple camera; it's got a button for taking a picture, a rotary switch that powers it on, and a lot of machinery and controls for dealing with film.  But all I really care about, for now, is the single button.  How should I go about capturing a button press? 

### But What Button? 

The actual button on the SRT is a simple mechanism, but not in the least bit digitally inclined.  So, I was forced to look at what buttons are available in the makerspace that can interact with a Raspberry Pi or a microcontroller.  I scoured [Sparkfun](https://www.sparkfun.com/categories/313) and [Adafruit](https://www.adafruit.com/category/235), looking for the right button. But everything that came up as a button, really looked too flimsy or cheap, and not like something that would stand up to a million presses.  However, I eventually found the [Cherry MX switch](https://www.cherrymx.de/en/mx-original/mx-blue.html), which is commonly used in fancy/DIY keyboards.  They are renowned for their durability, plus I could get ahold of a [breakout](https://www.sparkfun.com/products/13773) that makes connecting to the Cherry MX super easy.  

### Now How Do I Capture A Button Press?

There are two possible scenarios that I considered: 
1. Using the Raspberry Pi GPIO pins
2. Using a seperate microcontroller 
