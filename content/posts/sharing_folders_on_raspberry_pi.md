---
title: "Sharing Files From Your Raspberry Pi - With Samba"
date: 2021-02-21T22:21:14-08:00
draft: true
tags:
- Linux
- Networking
- HowTo
---

Sometimes, you need to share.  In my case, I needed to share photos from my Raspberry Pi Zero W, to tell if my prototype camera was working properly.  I considered NFS, but my first attempts didn't work and I'm horribly lazy.  My second thought was Samba, since I'd used that in the past and it was a good bit easier to set up.  And, it's discoverable!

# (Pi) Create your shared directory

# (Pi) Install samba packages
```bash
sudo apt-get update
sudo apt-get install samba samba-common-bin 
```

If you're prompted to enable DHCP, select "yes".

# (Pi) Create Samba user
This step was missing from lots of older "how-to" articles on Samba setups, and I'd never encountered it in the past, so I want to make sure nobody else gets frustrated by failed logins. 

```bash 
sudo smbpasswd -a <username>
```
