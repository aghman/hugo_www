HUGO = /usr/local/bin/hugo

.PHONY: build build-draft serve clean publish
build-draft:
	$(HUGO) -D

build: 
	$(HUGO)

serve:
	$(HUGO) server -D

publish: build
	./scripts/publish.sh /var/www


clean: 
	rm -rf public



