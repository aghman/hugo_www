#!/bin/bash
UUID=$(uuidgen)
PUBLISH_PATH=$1
NEW_DIR="$PUBLISH_PATH/austinghill-$UUID"
LINK_DIR="$PUBLISH_PATH/austinghill"

SSH_CMD="ssh ahill1979#kellygoodman.com@orion.hostineer.com"
echo "Publishing to $NEW_DIR"

# run prep on server, make unique staging directory in /var/www
echo "$SSH_CMD mkdir $NEW_DIR"
$SSH_CMD mkdir $NEW_DIR
# scp to unique staging directory
echo "scp -r ./public/* ahill1979@kellygoodman.com@orion.hostineer.com:$NEW_DIR"
scp -r ./public/* ahill1979@kellygoodman.com@orion.hostineer.com:$NEW_DIR


# Relink 
echo "$SSH_CMD mv -f $NEW_DIR $LINK_DIR"
$SSH_CMD rm -rf ${LINK_DIR}_bak
$SSH_CMD mv -f $LINK_DIR ${LINK_DIR}_bak
$SSH_CMD mv -f $NEW_DIR $LINK_DIR

# Cleanup old directories
echo "Cleaning up old directories..."
$SSH_CMD "cd $PUBLISH_PATH; ls $PUBLISH_PATH | grep \"austinghill-\" | grep -v $UUID | xargs -I % sh -c 'echo "Removing %"; rm -rf %'"
